package zetokz.webacademy.lesson2;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 * Created by zetokz on 08.08.16.
 */
abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = "BaseActivity";

    protected Button mBtnNextActivity;
    protected TextView mTvStatus;
    protected TextView mTvStack;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTvStack = (TextView) findViewById(R.id.tvStack);
        mTvStatus = (TextView) findViewById(R.id.tvStatus);
        mBtnNextActivity = (Button) findViewById(R.id.btnNextActivity);
        mBtnNextActivity.setOnClickListener(view -> onClickNextActivity());

        final ActionBar toolbar = getSupportActionBar();
        if (toolbar != null) {
            toolbar.setTitle(getName());
            toolbar.setDisplayShowTitleEnabled(true);
            toolbar.setDisplayHomeAsUpEnabled(true);
        }

        showCurrentTaskActivities(mTvStatus, this);

        logD("onCreate()");
    }

    protected void showCurrentTaskActivities(final TextView textView, final Context ctx) {
        final ActivityManager m = (ActivityManager) ctx.getSystemService(ctx.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningTaskInfo> runningTaskInfoList = m.getRunningTasks(1);
        final Iterator<ActivityManager.RunningTaskInfo> itr = runningTaskInfoList.iterator();
        while (itr.hasNext()) {
            final ActivityManager.RunningTaskInfo runningTaskInfo = itr.next();
            final int id = runningTaskInfo.id;
            final CharSequence desc = runningTaskInfo.description;
            final int numOfActivities = runningTaskInfo.numActivities;
            final String topActivity = runningTaskInfo.topActivity.getShortClassName();

            textView.setText(String.format("Activity: %s,\n Name: %s,\n Count: %d", topActivity, getName(), numOfActivities));
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        logD("onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        logD("onResume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        logD("onRestart()");
    }

    @Override
    protected void onStop() {
        super.onStop();

        logD("onStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();

        logD("onPause()");
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);

        logD("onNewIntent()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        logD("onDestroy()");
    }

    private void logD(final String s) {
        Log.d(TAG, String.format("LifeCycle: %s, ActivityName: %s, HashCode: %s", s, getName(), hashCode()));
    }

    protected abstract String getName();
    protected abstract void onClickNextActivity();

}
