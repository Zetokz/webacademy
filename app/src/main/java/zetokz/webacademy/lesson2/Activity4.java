package zetokz.webacademy.lesson2;

import android.os.Bundle;
import android.view.View;

public class Activity4 extends BaseActivity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBtnNextActivity.setVisibility(View.GONE);
    }

    @Override
    protected String getName() {
        return "D";
    }

    @Override
    protected void onClickNextActivity() {
//        final Intent intent = new Intent(this, Activity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
//        startActivity(intent);
    }
}
