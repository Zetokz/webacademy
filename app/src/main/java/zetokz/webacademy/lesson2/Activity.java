package zetokz.webacademy.lesson2;

import android.content.Intent;

public class Activity extends BaseActivity {

    @Override
    protected String getName() {
        return "A";
    }

    @Override
    protected void onClickNextActivity() {
        final Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }
}
