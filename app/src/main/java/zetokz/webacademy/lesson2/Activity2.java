package zetokz.webacademy.lesson2;

import android.content.Intent;

public class Activity2 extends BaseActivity {

    @Override
    protected String getName() {
        return "B";
    }

    @Override
    protected void onClickNextActivity() {
        Intent intent = new Intent(this, Activity3.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

}
