package zetokz.webacademy.lesson2;

import android.app.*;
import android.app.Activity;
import android.os.Bundle;

/**
 * Created by zetokz on 13.08.16.
 */
public class App extends Application implements Application.ActivityLifecycleCallbacks {

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(final Activity activity, final Bundle bundle) {

    }

    @Override
    public void onActivityStarted(final Activity activity) {

    }

    @Override
    public void onActivityResumed(final Activity activity) {

    }

    @Override
    public void onActivityPaused(final Activity activity) {

    }

    @Override
    public void onActivityStopped(final Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(final Activity activity) {

    }
}
