package zetokz.webacademy.lesson2;

import android.content.Intent;

public class Activity3 extends BaseActivity {

    @Override
    protected String getName() {
        return "C";
    }

    @Override
    protected void onClickNextActivity() {
        final Intent intent = new Intent(this, Activity4.class);
        startActivity(intent);
    }
}
